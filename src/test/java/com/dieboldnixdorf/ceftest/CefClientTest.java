package com.dieboldnixdorf.ceftest;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class CefClientTest {

	WebDriver cefClientDriver;
	WebDriverWait fluentWait;
	Process cefClientProcess;

	@Test
	public void googleTest() {
		System.out.println("Checking Windows::");
		Set<String> windowHandlesSet = cefClientDriver.getWindowHandles();
		windowHandlesSet.forEach(System.out::println);		
//		System.out.println("Navigating to the webpage");
//		cefClientDriver.get("http://www.google.com/xhtml");
//		System.out.println("Navigated to the webpage");
		sleep(5000);
		WebElement searchBox = null;
		for(String windowHandle : windowHandlesSet) {
			System.out.println("Starting Handle :: " + windowHandle);
			cefClientDriver.switchTo().window(windowHandle);
			System.out.println("The title is :: " + cefClientDriver.getTitle());
			try{
				searchBox = cefClientDriver.findElement(By.name("q")); 
			}catch (WebDriverException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			System.out.println("Ending Handle :: " + windowHandle);
			if(searchBox != null) break;
		}
		
		System.out.println("Searching on the web page");
		//Assert.assertTrue(searchBox.isDisplayed(), "The Search Box is not displayued");
		searchBox.sendKeys("ChromeDriver");
		searchBox.submit();
		System.out.println("Completed search on the web page");
		sleep(10000); // Let the user actually see something!
	}

	@BeforeClass
	public void beforeClass() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\development_wrk\\cef-test\\chromedriver_win32\\chromedriver.exe");
		//startCefProcess();
		ChromeOptions options = new ChromeOptions();
		options.setBinary(
				"C:\\development_wrk\\cef-test\\cef_binary_3.3578.1863.gbf8cff2_windows64_client\\Release\\cefclient.exe");
		options.addArguments("remote-debugging-port=12345");
		options.setCapability("debuggerAddress", "127.0.0.1:12345");
		cefClientDriver = new ChromeDriver(options);
	}

	private void startCefProcess() {
		ProcessBuilder cefClientProcessBuilder = new ProcessBuilder(
				"C:\\development_wrk\\cef-test\\cef_binary_3.3578.1863.gbf8cff2_windows64_client\\Release\\cefclient.exe",
				"--remote-debugging-port=12345");
		try {
			cefClientProcess = cefClientProcessBuilder.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public void afterClass() {
		cefClientDriver.quit();
		//cefClientProcess.destroy();
	}

	static void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}
}
